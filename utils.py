'''MIT License
Copyright (C) 2020 Prokofiev Kirill
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.'''

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.nn.parameter import Parameter


class AngleSimpleLinear(nn.Module):
    """Computes cos of angles between input vectors and weights vectors"""

    def __init__(self, in_features, out_features):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        self.weight.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)

    def forward(self, x):
        cos_theta = F.normalize(x, dim=1).mm(F.normalize(self.weight, dim=0))
        return (cos_theta.clamp(-1.0 + 1e-7, 1.0 - 1e-7), )


def focal_loss(input_values, gamma):
    """Computes the focal loss"""
    p = torch.exp(-input_values)
    loss = (1 - p) ** gamma * input_values
    return loss.mean()


def label_smoothing(classes, y_hot, smoothing=0.1):
    lam = 1 - smoothing
    new_y = torch.where(y_hot.bool(), lam, smoothing/(classes-1))
    return new_y


class AMSoftmaxLoss(nn.Module):
    """Computes the AM-Softmax loss with cos or arc margin"""
    margin_types = ['cos', 'arc', 'cross_entropy']

    def __init__(self, margin_type='cos', device='cpu', num_classes=2,
                 label_smooth=False, smoothing=0.1, ratio=(1, 1), gamma=0.,
                 m=0.5, s=30, t=1.):
        super().__init__()
        assert margin_type in AMSoftmaxLoss.margin_types
        self.margin_type = margin_type
        self.classes = num_classes
        assert gamma >= 0
        self.gamma = gamma
        assert m >= 0
        self.m = torch.Tensor([m/i for i in ratio]).to(device)
        assert s > 0
        if self.margin_type in ('arc', 'cos',):
            self.s = s
        else:
            assert self.margin_type == 'cross_entropy'
            self.s = 1
        assert t >= 1
        self.t = t
        self.label_smooth = label_smooth
        self.smoothing = smoothing

    def forward(self, cos_theta, target):
        ''' target - one hot vector '''
        if isinstance(cos_theta, tuple):
            cos_theta = cos_theta[0]
        if self.label_smooth:
            target = label_smoothing(
                classes=self.classes, y_hot=target, smoothing=self.smoothing)
        if self.margin_type in ('cos', 'arc',):
            # fold one_hot to one vector [batch size] (need to do it when label smooth or augmentations used)
            fold_target = target.argmax(dim=1)
            # unfold it to one-hot()
            one_hot_target = F.one_hot(fold_target, num_classes=self.classes)
            m = self.m * one_hot_target
            if self.margin_type == 'cos':
                phi_theta = cos_theta - m
                output = phi_theta
            elif self.margin_type == 'arc':
                theta = torch.acos(cos_theta)
                phi_theta = torch.cos(theta + self.m)
                output = phi_theta
        else:
            assert self.margin_type == 'cross_entropy'
            output = cos_theta

        if self.gamma == 0 and self.t == 1.:
            pred = F.log_softmax(self.s*output, dim=-1)
            return torch.mean(torch.sum(-target * pred, dim=-1))

        pred = F.log_softmax(self.s*output, dim=-1)
        return focal_loss(torch.sum(-target * pred, dim=-1), self.gamma)


def contrast_depth_conv(input, device='cpu'):
    ''' compute contrast depth in both of (out, label) '''
    '''
        input  32x32
        output 8x32x32
    '''

    kernel_filter_list = [
        [[1, 0, 0], [0, -1, 0], [0, 0, 0]], [[0, 1, 0], [0, -1, 0],
                                             [0, 0, 0]], [[0, 0, 1], [0, -1, 0], [0, 0, 0]],
        [[0, 0, 0], [1, -1, 0], [0, 0, 0]
         ], [[0, 0, 0], [0, -1, 1], [0, 0, 0]],
        [[0, 0, 0], [0, -1, 0], [1, 0, 0]], [[0, 0, 0], [0, -1,
                                                         0], [0, 1, 0]], [[0, 0, 0], [0, -1, 0], [0, 0, 1]]
    ]

    kernel_filter = np.array(kernel_filter_list, np.float32)

    kernel_filter = torch.from_numpy(
        kernel_filter.astype(np.float64)).float().to(device)
    # weights (in_channel, out_channel, kernel, kernel)
    kernel_filter = kernel_filter.unsqueeze(dim=1)

    input = input.unsqueeze(dim=1).expand(
        input.shape[0], 8, input.shape[1], input.shape[2])

    contrast_depth = F.conv2d(
        input, weight=kernel_filter, groups=8)  # depthwise conv

    return contrast_depth


# Pearson range [-1, 1] so if < 0, abs|loss| ; if >0, 1- loss
class Contrast_depth_loss(nn.Module):
    def __init__(self, device='cpu'):
        super(Contrast_depth_loss, self).__init__()
        self.device = device
        return

    def forward(self, out, label):
        '''
        compute contrast depth in both of (out, label),
        then get the loss of them
        tf.atrous_convd match tf-versions: 1.4
        '''
        contrast_out = contrast_depth_conv(out, device=self.device)
        contrast_label = contrast_depth_conv(label, device=self.device)

        criterion_MSE = nn.MSELoss().to(self.device)

        loss = criterion_MSE(contrast_out, contrast_label)
        # loss = torch.pow(contrast_out - contrast_label, 2)
        # loss = torch.mean(loss)

        return loss


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def multi_task_criterion(output: tuple, target: tuple,
                         C: float = 1.,
                         Cs: float = 0.25,
                         Ci: float = 0.1,
                         Cf: float = 1.,
                         Cd: float = 0.2,
                         device='cpu', eval=False):
    ''' output -> tuple of given losses
    target -> torch tensor of a shape [batch*num_tasks]
    return loss function '''

    softmax = AMSoftmaxLoss(m=0.5,
                            s=1,
                            margin_type='cross_entropy',
                            label_smooth=False,
                            smoothing=0.1,
                            ratio=[1, 1],
                            gamma=0)
    cross_entropy = AMSoftmaxLoss(margin_type='cross_entropy',
                                  label_smooth=False,
                                  smoothing=0.1,
                                  gamma=0)
    bce = nn.BCELoss()
    cdc = Contrast_depth_loss(device=device)

    spoof_target = F.one_hot(target[0].to(torch.int64), num_classes=2)
    spoof_type_target = F.one_hot(target[1].to(torch.int64), num_classes=11)
    lightning_target = F.one_hot(target[2].to(torch.int64), num_classes=5)

    mask = target[0] == 0
    # filter output and target for real images
    filtered_output = torch.sigmoid(output[3][mask])
    filtered_target = output[3][mask].type(torch.float32)

    # compute label losses

    real_atr_loss = bce(filtered_output, filtered_target)
    if eval:
        return real_atr_loss
    spoof_loss = softmax(output[0], spoof_target.to(device))
    spoof_type_loss = cross_entropy(output[1][:], spoof_type_target.to(device))
    lightning_loss = cross_entropy(output[2], lightning_target.to(device))
    # loss of face (real attribute)

    # compute depth and ref losses
    depth_loss = cdc(output[4].squeeze(dim=1),
                     target[4].squeeze(dim=1).to(device))
    # combine losses
    loss = C * spoof_loss + Cs * spoof_type_loss + Ci * lightning_loss + \
        Cf * real_atr_loss + Cd * depth_loss
    return loss


def get_metrics_test(TP, TN, FP, FN):
    APCER = FP / (FP + TN)
    BPCER = FN / (FN + TP)
    ACER = (APCER + BPCER) / 2
    acc = (TP + TN) / (TP + TN + FP + FN)
    TPR = TP / (TP + FN)
    FRR = FN / (TP + FN)
    return [acc, TPR, FRR, APCER, BPCER, ACER]


if __name__ == '__main__':
    output = (torch.randn(32, 2), torch.randn(32, 11),
              torch.randn(32, 5), torch.randn(32, 40))
    depth_map = torch.randn(32, 1, 32, 32)
    output = (output[0], output[1], output[2], output[3], depth_map)

    target = (torch.randint(0, 2, (32,)), torch.randint(0, 11, (32,)),
              torch.randint(0, 5, (32,)), torch.randint(0, 40, (32,)))
    depth_map = torch.randn(32, 1, 32, 32)
    target = (target[0], target[1], target[2], target[3], depth_map)
    # print(target.shape)
    loss = multi_task_criterion(output, target)
    metric = get_metrics_test(4, 3, 2, 1)
    print(loss, metric)
