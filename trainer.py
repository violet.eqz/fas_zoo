import torch
from tqdm import tqdm

from sklearn.metrics import roc_auc_score, accuracy_score
from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.metrics import confusion_matrix
import numpy as np
import os


class Trainer():
    ''' Trainer class for training the model,
        Use wandb for logging and saving checkpoints.
        Args:
            model: model to train
            optimizer: optimizer to use
            scheduler: scheduler to use
            device: device to use
    '''

    def __init__(self, model, optimizer, criterion, device='cpu'):
        self.model = model
        self.optimizer = optimizer
        self.device = device
        self.criterion = criterion
        # dict of losses weights
        self.losses_weights = {
            'C': 1.,
            'Cs': 0.25,
            'Ci': 0.1,
            'Cf': 1.,
            'Cd': 0.2,
        }

    def train(self, train_loader, epoch, simple=False, parallel=False):
        ''' Train the model for one epoch
            Args:
                train_loader: train data loader
                epoch: current epoch
        '''
        self.model.train()
        results = {
            'loss': [],
            'TP': 0,
            'TN': 0,
            'FP': 0,
            'FN': 0,
        }
        pbar = tqdm(train_loader)
        for _, (data, target) in enumerate(pbar):
            # set gradients to zero
            self.optimizer.zero_grad()
            # move data to device
            data, target = data.to(self.device), target
            # forward pass
            # check if data is in [0-1] range
            if data.max() > 1:
                data = data.float()
                data = data / 255
            if parallel:
                output = self.model.module._train_logits(data)
            else:
                output = self.model._train_logits(data)
            # compute loss
            loss = self.criterion(
                output, target,
                C=self.losses_weights['C'],
                Cs=self.losses_weights['Cs'],
                Ci=self.losses_weights['Ci'],
                Cf=self.losses_weights['Cf'],
                Cd=self.losses_weights['Cd'],
                device=self.device,
                eval=simple)
            loss.backward()
            self.optimizer.step()
            # special case for using ReduceLROnPlateau scheduler
            # get accuracy and roc_auc_score for spoof/live classification
            spoof_target = target[0].cpu().numpy()
            spoof_output = output[0].cpu().detach().numpy()
            spoof_output = np.argmax(spoof_output, axis=1)
            results['loss'] = np.append(results['loss'], loss.item())
            # GET TP, TN, FP, FN
            results['TP'] += np.sum(np.logical_and(spoof_output ==
                                    1, spoof_target == 1))
            results['TN'] += np.sum(np.logical_and(spoof_output ==
                                    0, spoof_target == 0))
            results['FP'] += np.sum(np.logical_and(spoof_output ==
                                    1, spoof_target == 0))
            results['FN'] += np.sum(np.logical_and(spoof_output ==
                                    0, spoof_target == 1))
            # _get_metrics returns the following:
            metrics = self._get_metrics(spoof_target, spoof_output)
            # log metrics: only loss, acc and TP, TN, FP, FN
            pbar.set_postfix({'mode': 'TRAIN', 'loss': loss.item(),
                              'acc': metrics[0], 'TP': results['TP'], 'TN': results['TN'], 'FP': results['FP'], 'FN': results['FN']})
        # save checkpoint
        if os.path.exists('checkpoints') is False:
            os.mkdir('checkpoints')
        torch.save(self.model.state_dict(), f'checkpoints/train_{epoch}.pth')
        return results

    def eval(self, val_loader, epoch, parallel=False):
        ''' Evaluate the model
            Args:
                val_loader: validation data loader
                epoch: current epoch
        '''
        self.model.eval()
        results = {
            'loss': [],
            'TP': 0,
            'TN': 0,
            'FP': 0,
            'FN': 0,
        }
        pbar = tqdm(val_loader)
        for _, (data, target) in enumerate(pbar):
            # move data to device
            data, target = data.to(self.device), target
            if data.max() > 1:
                data = data.float()
                data = data / 255
            # forward pass
            with torch.no_grad():
                if parallel:
                    output = self.model.module._train_logits(data)
                else:
                    output = self.model._train_logits(data)
            # compute loss
            loss = self.criterion(
                output, target,
                C=self.losses_weights['C'],
                Cs=self.losses_weights['Cs'],
                Ci=self.losses_weights['Ci'],
                Cf=self.losses_weights['Cf'],
                Cd=self.losses_weights['Cd'],
                device=self.device,
                eval=True)
            # get accuracy and roc_auc_score for spoof/live classification
            spoof_target = target[0].cpu().numpy()
            spoof_output = output[0].cpu().detach().numpy()
            spoof_output = np.argmax(spoof_output, axis=1)
            results['loss'] = np.append(results['loss'], loss.item())
            # GET TP, TN, FP, FN
            results['TP'] += np.sum(np.logical_and(spoof_output ==
                                    1, spoof_target == 1))
            results['TN'] += np.sum(np.logical_and(spoof_output ==
                                    0, spoof_target == 0))
            results['FP'] += np.sum(np.logical_and(spoof_output ==
                                    1, spoof_target == 0))
            results['FN'] += np.sum(np.logical_and(spoof_output ==
                                    0, spoof_target == 1))
            # _get_metrics returns the following:
            # acc, roc, f1, precision, recall, confusion, APCER, BPCER, ACER
            metrics = self._get_metrics(spoof_target, spoof_output)
            # log metrics
            pbar.set_postfix({'mode': 'VAL', 'loss': loss.item(),
                              'acc': metrics[0], 'TP': results['TP'], 'TN': results['TN'], 'FP': results['FP'], 'FN': results['FN']})
        # save checkpoint
        if os.path.exists('checkpoints') is False:
            os.mkdir('checkpoints')
        torch.save(self.model.state_dict(), f'checkpoints/train_{epoch}.pth')
        return results

    def test(self, test_loader, epoch, parallel=False):
        self.model.eval()
        TP = 0
        TN = 0
        FP = 0
        FN = 0
        pbar = tqdm(test_loader)
        for _, (data, target) in enumerate(pbar):
            data, target = data.to(self.device), target
            if data.max() > 1:
                data = data.float()
                data = data / 255
            with torch.no_grad():
                if parallel:
                    output = self.model.module._get_output(data)
                else:
                    output = self.model._get_output(data)
            # Output = [B,2]
            # Is softmax probability of spoof/live
            # Define threshold for spoof/live
            # If spoof > threshold, then spoof
            # else live
            Threshold = 0.8
            output = output[:, 1]
            output = output.cpu().detach().numpy()
            output = np.where(output > Threshold, 1, 0)
            target = target.cpu().detach().numpy()

            # Get TP, TN, FP, FN
            TP += np.sum(np.logical_and(output == 1, target == 1))
            TN += np.sum(np.logical_and(output == 0, target == 0))
            FP += np.sum(np.logical_and(output == 1, target == 0))
            FN += np.sum(np.logical_and(output == 0, target == 1))
            pbar.set_postfix({'TP': TP, 'TN': TN, 'FP': FP, 'FN': FN})
        # Get APCER, BPCER, ACER
        APCER = FP / (FP + TN)
        BPCER = FN / (FN + TP)
        ACER = (APCER + BPCER) / 2
        # Get ROC, F1, Precision, Recall, Accuracy
        # Get confusion matrix
        acc = (TP + TN) / (TP + TN + FP + FN)
        FPR = FP / (FP + TN)
        print('FPR: ', FPR)
        print('Accuracy: ', acc)
        print(f'APCER: {APCER}, BPCER: {BPCER}, ACER: {ACER}')
        return APCER, BPCER, ACER

    def _get_metrics(self, target, output):
        '''
        Get metrics for spoof/live classification
        Args:
            target: target labels
            output: output of the model
        Returns (in a list):
            acc: accuracy
            roc: roc_auc_score
            f1: f1_score
            precision: precision_score
            recall: recall_score
            confusion: confusion_matrix
            APCER: Attack Presentation Classification Error Rate
            BPCER: Bona Fide Presentation Classification Error Rate
            ACER: Average Classification Error Rate
        '''
        acc = accuracy_score(target, output)
        roc = roc_auc_score(target, output)
        f1 = f1_score(target, output)
        precision = precision_score(target, output)
        recall = recall_score(target, output)
        confusion = confusion_matrix(target, output)
        # get TP, TN, FP, FN
        TP = confusion[1][1]
        TN = confusion[0][0]
        FP = confusion[0][1]
        FN = confusion[1][0]
        # Get APCER, BPCER, ACER
        APCER = FP / (TN + FP)
        BPCER = FN / (TP + FN)
        ACER = (APCER + BPCER) / 2
        return [acc, roc, f1, precision, recall, confusion, APCER, BPCER, ACER]

    def _get_metrics_test(self, TP, TN, FP, FN):
        APCER = FP / (FP + TN)
        BPCER = FN / (FN + TP)
        ACER = (APCER + BPCER) / 2
        acc = (TP + TN) / (TP + TN + FP + FN)
        TPR = TP / (TP + FN)
        FRR = FN / (TP + FN)
        return [acc, TPR, FRR, APCER, BPCER, ACER]
