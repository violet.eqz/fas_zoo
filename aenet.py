import torch.nn as nn
import torch
import math
import torch.utils.model_zoo as model_zoo
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
from utils import AngleSimpleLinear


class SELayer(nn.Module):
    def __init__(self, channel, reduction=8):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction),
            nn.Hardswish(inplace=True),
            nn.Linear(channel // reduction, channel),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y


class Conv2d_cd(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1,
                 padding=1, dilation=1, groups=1, bias=False, theta=0.7):

        super(Conv2d_cd, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size,
                              stride=stride, padding=padding, dilation=dilation, groups=groups, bias=bias)
        self.theta = theta

    def forward(self, x):
        out_normal = self.conv(x)

        if math.fabs(self.theta - 0.0) < 1e-8:
            return out_normal
        else:
            # pdb.set_trace()
            [C_out, C_in, kernel_size, kernel_size] = self.conv.weight.shape
            kernel_diff = self.conv.weight.sum(2).sum(2)
            kernel_diff = kernel_diff[:, :, None, None]
            out_diff = F.conv2d(input=x, weight=kernel_diff, bias=self.conv.bias,
                                stride=self.conv.stride, padding=0, groups=self.conv.groups)

            return out_normal - self.theta * out_diff


def conv3x3(in_planes, out_planes, stride=1):
    "3x3 convolution with padding"
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)


class InvertedResidual(nn.Module):

    def __init__(self, inp, oup, stride, expand_ratio, downsample=None, id=0):
        super(InvertedResidual, self).__init__()
        self.stride = stride
        assert stride in [1, 2]
        self.downsample = downsample
        # print(id)
        hidden_dim = round(inp * expand_ratio)
        self.use_res_connect = self.stride == 1 and inp == oup

        if expand_ratio == 1:
            self.conv = nn.Sequential(
                # dw
                Conv2d_cd(hidden_dim, hidden_dim, 3, stride,
                          1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.Hardswish(inplace=True),
                # pw-linear
                Conv2d_cd(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )
        else:
            self.conv = nn.Sequential(
                # pw
                nn.Conv2d(inp, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.Hardswish(inplace=True),
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride,
                          1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.Hardswish(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )

    def forward(self, x):
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            if self.downsample is not None:
                return self.downsample(x) + self.conv(x)
            else:
                return self.conv(x)


# AENet_C,S,G is based on ResNet-18


class AENet(nn.Module):

    def __init__(self, input_channel=32, width_mult=1.,
                 interverted_residual_setting=[
            # t, c, n, s
            [1, 16, 1, 2],
            [6, 32, 3, 2],  # 56x56
            [6, 64, 6, 2],  # 14x14
            [6, 96, 4, 2],  # 7x7
                     ]):
        self.input_channel = int(input_channel * width_mult)
        self.interverted_residual_setting = interverted_residual_setting

        super(AENet, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, self.input_channel, 3, 2, 1),
            nn.BatchNorm2d(self.input_channel),
            # hard swish activation
            nn.Hardswish(inplace=True),
        )
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        # self.layer1 = self._make_layer(block, 64, layers[0])
        # self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        # self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        # self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.layer1 = nn.Sequential(self._make_layer(
            InvertedResidual, 1))
        self.layer2 = nn.Sequential(self._make_layer(
            InvertedResidual, 2))
        self.layer3 = nn.Sequential(self._make_layer(
            InvertedResidual, 3))
        self.layer4 = nn.Sequential(self._make_layer(
            InvertedResidual, 4))

        # Classifiers of semantic informantion
        self.fc_face = self._make_fc(
            self.interverted_residual_setting[-1][1], 40)
        self.fc_attack = self._make_fc(
            self.interverted_residual_setting[-1][1], 11)
        self.fc_light = self._make_fc(
            self.interverted_residual_setting[-1][1], 5)
        self.fc_live = self._make_fc(
            self.interverted_residual_setting[-1][1], 2, bin=True)

        # self.fc_face = nn.Linear(
        #     self.interverted_residual_setting[-1][1], 40)
        # self.fc_attack = nn.Linear(
        #     self.interverted_residual_setting[-1][1], 11)
        # self.fc_light = nn.Linear(
        #     self.interverted_residual_setting[-1][1], 5)
        # self.fc_live = nn.Linear(
        #     self.interverted_residual_setting[-1][1], 2)

        # Two embedding modules of geometric information
        self.depth = nn.Sequential(
            nn.Conv2d(self.interverted_residual_setting[-1][1],
                      1, kernel_size=3, stride=1, padding=1, bias=False),
            nn.Upsample((14, 14), mode='bilinear'),
            # The ground truth of depth map and reflection map has been normalized[torchvision.transforms.ToTensor()]
            nn.Sigmoid(),
        )

        # initialization
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(
                    m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, Conv2d_cd):
                nn.init.kaiming_normal_(
                    m.conv.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight,
                                  1)

    def _make_fc(self, output_channel, class_num, bin=False):
        if not bin:
            return nn.Sequential(
                # Dropout
                nn.Dropout(p=0.15),
                nn.BatchNorm1d(output_channel),
                # hard swish activation
                nn.Hardswish(inplace=True),
                nn.Linear(output_channel, class_num),
            )
        else:
            return nn.Sequential(
                # Dropout
                nn.Dropout(p=0.15),
                nn.BatchNorm1d(output_channel),
                # hard swish activation
                nn.Hardswish(inplace=True),
                AngleSimpleLinear(output_channel, class_num),
            )

    def _make_layer(self, block, layer_no):
        t, c, n, s = self.interverted_residual_setting[layer_no-1]
        output_channel = int(c)
        layer = []
        for i in range(n):
            downsample = None
            if i == 0:
                downsample = nn.Sequential(nn.AvgPool2d(2, stride=2),
                                           nn.BatchNorm2d(
                    self.input_channel),
                    nn.Conv2d(
                    self.input_channel, output_channel, kernel_size=1, bias=False)
                )
                layer.append(
                    block(self.input_channel, output_channel, s, expand_ratio=t, downsample=downsample))
            else:
                layer.append(
                    block(self.input_channel, output_channel, 1, expand_ratio=t, downsample=downsample))
            self.input_channel = output_channel
        layer.append(SELayer(self.input_channel))
        return nn.Sequential(*layer)

    # def _make_layer(self, block, planes, blocks, stride=1):
    #     downsample = None
    #     if stride != 1 or self.inplanes != planes * block.expansion:
    #         downsample = nn.Sequential(
    #             nn.Conv2d(self.inplanes, planes * block.expansion,
    #                       kernel_size=1, stride=stride, bias=False),
    #             self.BN(planes * block.expansion),
    #         )

    #     layers = []
    #     layers.append(block(self.inplanes, planes, stride, downsample))
    #     self.inplanes = planes * block.expansion
    #     for i in range(1, blocks):
    #         layers.append(block(self.inplanes, planes))

    #     return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        # print(x.shape)
        x = self.fc_live(x)[0]

        return x

    def _train_logits(self, x):
        '''
        Return the logits of the network for training multi-task learning
        Returned the losses in a list as follow:
        [x_live, x_attack, x_light, x_face, x_depth]
        '''
        x = self.conv1(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x_depth = self.depth(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)

        x_face = torch.sigmoid(self.fc_face(x))
        x_attack = self.fc_attack(x)
        x_light = self.fc_light(x)
        x_live = self.fc_live(x)[0]

        return [x_live, x_attack, x_light, x_face, x_depth]

    def _get_output(self, x):
        '''
        Return the output of the network for inference
        '''
        x = self.conv1(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.fc_live(x)
        if isinstance(x, tuple):
            x = x[0]
        probab = F.softmax(x, dim=-1)
        return probab


if __name__ == "__main__":
    import torchsummary as summary
    model = AENet()
    sample = torch.rand((16, 3, 224, 224))
    # print(model(sample))
    summary.summary(model, (3, 224, 224), device='cpu')
    # x = model(sample)
    # print(x)
    model.eval()
    x = model._get_output(sample)
    print(x.shape)
    model.train()
    # face, attack, light, live, depth = model._train_logits(sample)
    x = model._train_logits(sample)
    x = model(sample)
